#define CATCH_CONFIG_MAIN

#include "lib/single_include/catch2/catch.hpp"

TEST_CASE("Booleans are set to the correct value", "[boolean]") {
    REQUIRE(true == 1);
    REQUIRE(false == 0);
}
