#include "WifiHelper.h"

void WifiHelper::init() {
    // Open kernel socket:
    sockFd = iw_sockets_open();

    if (sockFd < 0) {
        log_error(LOGGER_PREFIX + "Failed to open kernel socket!");
        exit(1);
    }
}

void WifiHelper::reset() {
    if (sockFd >= 0) {
        iw_sockets_close(sockFd);
        sockFd = -1;
    }
}

void WifiHelper::printScanResult(wireless_scan_head* scanResult) {
    wireless_scan* result = scanResult->result;
    while (NULL != result) {
        printf("%s\n", result->b.essid);
        result = result->next;
    }
}

bool WifiHelper::hasScanResultSsid(wireless_scan_head* scanResult, const char* ssid) {
    wireless_scan* result = scanResult->result;
    while (NULL != result) {
        if (strcmp(result->b.essid, ssid) == 0) {
            return true;
        }
        result = result->next;
    }
    return false;
}

bool WifiHelper::scan(const char* interface, wireless_scan_head* scanResult) {
    // Get scan metadata:
    iwrange range;
    if (iw_get_range_info(sockFd, interface, &range)) {
        log_error(LOGGER_PREFIX + "Failed to get range info!");
        return false;
    }

    // Scan:
    if (iw_scan(sockFd, const_cast<char*>(interface), range.we_version_compiled, scanResult) < 0) {
        log_error(LOGGER_PREFIX + "Failed to perform iw_scan!");
        return false;
    }
    return true;
}

bool WifiHelper::tryConnect(const char* interface, const char* ssid, const char* pw) {
    if (!isConnectedTo(interface, ssid)) {
        connect(interface, ssid, pw);
        return isConnectedTo(interface, ssid);
    }
    return true;
}

void WifiHelper::connect(const char* interface, const char* ssid, const char* pw) {
    log_info(LOGGER_PREFIX + "Connecting to Wifi " + std::string(ssid) + " ...");
    std::string command = "bash src/scripts/connectToWifi.sh  \"" + std::string(interface) + "\" \"" + std::string(ssid) + "\" \"" + std::string(pw) + "\"";
    std::system(command.c_str());
    log_info(LOGGER_PREFIX + "Connected to Wifi " + std::string(ssid));
}

bool WifiHelper::isConnectedTo(const char* interface, const char* ssid) {
    std::string command = "bash src/scripts/checkIfConnected.sh \"" + std::string(interface) + "\" \"" + std::string(ssid) + "\" 1 0";
    int ret = std::system(command.c_str());
    return WEXITSTATUS(ret);
}

bool WifiHelper::scanForSsid(const char* interface, const char* ssid) {
    std::string command = "bash src/scripts/scanForSsid.sh \"" + std::string(interface) + "\" \"" + std::string(ssid) + "\" 1 0";
    int ret = std::system(command.c_str());
    // log_error("Exit code: " + std::to_string(WEXITSTATUS(ret)));
    return WEXITSTATUS(ret);
}
