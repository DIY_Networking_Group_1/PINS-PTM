#include "CacheManager.h"

CacheManager::CacheManager(int maxCacheSize) {
    this->cache = new Cache(maxCacheSize);
}

CacheManager::~CacheManager() {
    client.disconnectFromServer();
    delete cache;
}

void CacheManager::waitForSsid(WifiHelper* wifi, const char* interface, const char* ssid) {
    while (1) {
        log_info(LOGGER_PREFIX + "Scanning for SSID...");
        if (wifi->scanForSsid(interface, ssid)) {
            log_info(LOGGER_PREFIX + "Found SSID: " + std::string(ssid));
            return;
        }
        log_info(LOGGER_PREFIX + "Did not found SSID " + std::string(ssid) + ". Sleeping for 5 second...");
        usleep(5000000);
    }
}

bool CacheManager::connectToBaseStation() {
    return client.init() && client.connectToServer();
}

int CacheManager::sendMessage(PtMessage* msg) {
    unsigned char* buf = new unsigned char[msg->getMsgLength()];
    msg->genMsg(buf);
    int i = client.sendData(reinterpret_cast<char*>(buf), msg->getMsgLength());
    delete buf;
    return i;
}

bool CacheManager::readMessage(PtType type, PtMessage* msg, bool waitForNoMsgs) {
    char* buf = new char[TCP_MAX_READ_SIZE];
    int l = 0;
    do {
        int l = client.readData(buf, TCP_MAX_READ_SIZE);
        if (l > 0) {
            *msg = PtMessage(reinterpret_cast<unsigned char*>(buf), l);
            if (msg->getState() == i_success) {
                if (msg->getType() == type || (waitForNoMsgs && msg->getType() == pt_no_messages)) {
                    log_debug(LOGGER_PREFIX + "Received "  + std::to_string(l) + " byte(s) from base station.");
                    delete buf;
                    return true;
                } else {
                    log_warn(LOGGER_PREFIX + "Dropping read message - type = " + std::to_string(msg->getType()) + " requested = " + std::to_string(type));
                }
            } else {
                log_warn(LOGGER_PREFIX + "Dropping read message - parsig failed.");
            }
        } else {
            log_warn(LOGGER_PREFIX + "Dropping read message - l = " + std::to_string(l));
        }
    } while (l > 0);
    delete buf;
    return false;
}

bool CacheManager::shouldExchangeWith(PtMessage* msg) {
    if (baseStationAddr == NULL || !compArr(baseStationAddr.get(), msg->getTAddr(), 6)) {
        baseStationAddr = makeSharedUCharArrayPtr(msg->getTAddr(), 6);
        return true;
    } else {
        return true;
    }
}

void CacheManager::sendCache() {
    ToSendCache toSendCache;
    cache->getSendCache(baseStationAddr.get(), &toSendCache);

    log_info(LOGGER_PREFIX + "Started sending messages for " + std::to_string(toSendCache.size()) + " receiver(s).");
    PtMessage msg(pt_reserved);
    CacheObj c;
    CacheQueue_spt q = NULL;
    ADDR addr;
    for (uint i = 0; i < toSendCache.size(); i++) {
        q = toSendCache[i].second;
        addr = std::make_shared<std::array<unsigned char, 6>>();
        memcpy(addr.get()->data(), toSendCache[i].first.c_str(), 6);
        while (q.get()->size() > 0) {
            c = q.get()->pop();
            msg = PtMessage(pt_send_message, addr, c.data.get(), c.l);
            sendMessage(&msg);
            log_debug(LOGGER_PREFIX + "Send message with " + std::to_string(msg.getMsgLength()) + " bytes to base station.");

            // Wait for ACK:
            if (!readMessage(pt_ack_last_message, &msg, false)) {
                log_warn(LOGGER_PREFIX + "Failed to receive pt_ack_last_message.");
                return;
            }
            log_debug(LOGGER_PREFIX +  "Received ACK.");
        }
    }
    msg = PtMessage(pt_no_messages);
    sendMessage(&msg);
    log_info(LOGGER_PREFIX + "Sending cache finished.");
}

void CacheManager::receiveCache() {
    log_debug(LOGGER_PREFIX + "Started reading cache.");
    PtMessage msg(pt_reserved);
    while (readMessage(pt_send_message, &msg, true)) {
        if (msg.getType() == pt_no_messages) {
            log_info(LOGGER_PREFIX + "Finished reading messages. Received pt_no_messages.");
            return;
        }

        uchar_sp data = makeSharedUCharArrayPtr(msg.getPayload(), msg.getPayloadLength());
        cache->add(msg.getTAddr(), data, msg.getPayloadLength());
        log_debug(LOGGER_PREFIX +  "Received message from: " + std::string(reinterpret_cast<char*>(msg.getAddr().get())));

        if (cache->isFull()) {
            msg = PtMessage(pt_ptm_full);
            sendMessage(&msg);
            log_info(LOGGER_PREFIX + "Send cache is full to base station!");
            return;
        } else {
            msg = PtMessage(pt_ack_last_message);
            sendMessage(&msg);
            log_debug("Send ACK.");
        }
    }
    log_debug(LOGGER_PREFIX + "Finished reading cache.");
}

bool CacheManager::sendReceiveDataToBaseStation() {
    PtMessage msg = PtMessage(pt_get_base_addr);
    sendMessage(&msg);

    if (!readMessage(pt_send_base_addr, &msg, false)) {
        log_warn(LOGGER_PREFIX + "Failed to receive pt_send_base_addr.");
        return false;
    }
    log_debug(LOGGER_PREFIX + "Received addr.");

    if (!shouldExchangeWith(&msg)) {
        return false;
    }

    sendCache();
    receiveCache();
    return true;
}

void CacheManager::run(const char* interface, const char* ssid, const char* pw) {
    WifiHelper wifi;
    wifi.init();
    while (1) {
        waitForSsid(&wifi, interface, ssid);
        if (wifi.tryConnect(interface, ssid, pw)) {
            if (connectToBaseStation()) {
                sendReceiveDataToBaseStation();
                log_info(LOGGER_PREFIX + "Base station connection done. Sleeping for 10s...");
            } else {
                log_warn(LOGGER_PREFIX + "Base station connection failed. Sleeping for 10s...");
            }
            client.disconnectFromServer();
            usleep(10000000);
        } else {
            log_info(LOGGER_PREFIX + "Connecting to " + std::string(ssid) + " failed.");
        }
     }
}
