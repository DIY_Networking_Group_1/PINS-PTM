#include "Cache.h"

Cache::Cache(int maxCacheSize) {
    this->maxCacheSize = maxCacheSize;
    add("0000", makeSharedUCharArrayPtr("BlaBla"), 6);
}

void Cache::add(std::string addr, uchar_sp data, uint l) {
    CacheObj c;
    c.data = data;
    c.l = l;
    CacheMap::iterator it = map.find(addr);
    if (it == map.end()) {
        CacheQueue_spt ptr = std::make_shared<Queue<CacheObj>>();
        ptr->push(c);
        map[addr] = ptr;
    } else {
        map[addr]->push(c);
    }
    cacheSize++;
}

void Cache::add(uchar* addr, uchar_sp data, uint l) {
    add(std::string(reinterpret_cast<char*>(addr), 6), data, l);
}

bool Cache::isFull() {
    return cacheSize >= maxCacheSize;
}

void Cache::getSendCache(uchar* addr, ToSendCache* toSendCache) {
    for (auto it = map.cbegin(); it != map.cend();) {
        if (compArr(addr, reinterpret_cast<uchar*>(const_cast<char*>(it->first.c_str())), 6)) {
            toSendCache->push_back(std::make_pair(it->first, it->second));
            cacheSize-= it->second.get()->size();
            map.erase(it++);
        } else {
            it++;
        }
    }
}
