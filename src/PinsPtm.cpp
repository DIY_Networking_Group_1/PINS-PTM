#include "PinsPtm.h"

int main(int argc, const char* argv[]) {
    checkIfRunningAsRoot();

    Args args;
    parseArgs(argc, argv, &args);

    CacheManager manager = CacheManager(args.maxTransportCacheSize);
    manager.run(DEFAULT_INTERFACE, DEFAULT_BASE_STATION_SSID, DEFAULT_BASE_STATION_PW);
}

void parseArgs(int argc, const char* argv[], Args* argsRet) {
    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
            std::cout << HELPTEXT << std::endl;
            exit(0);
        } else if (strcmp(argv[i], "-c") == 0) {
            argsRet->maxTransportCacheSize = atoi(argv[++i]);
        }
    }
}

void checkIfRunningAsRoot() {
    uid_t uid = getuid();
    uid_t euid = geteuid();
    log_debug("uid: " + std::to_string(uid));
    log_debug("euid: " + std::to_string(euid));
    if (uid > 0 && uid == euid) {
        log_error("Not run as root. Try use sudo!");
        exit(-4);
    }
    log_info("Root check successfull.");
}
