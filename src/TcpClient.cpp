#include "TcpClient.h"

TcpClient::TcpClient(std::string hostAddr, uint16_t port) {
    this->hostAddr = hostAddr;
    this->port = port;
}

TcpClient::~TcpClient() {
    switch (state) {
        case tcp_connecting:
        case tcp_connected:
        case tcp_init:
            disconnectFromServer();
            break;
    }
}

TcpCState TcpClient::getState() {
    return state.load();
}

void TcpClient::setState(TcpCState newState) {
    if (state != newState) {
        log_debug(LOGGER_PREFIX + "New state: " + std::to_string(state) + " => " + std::to_string(newState));
        state = newState;
    }
}

bool TcpClient::connectToServer() {
    if (state != tcp_init) {
        log_error(LOGGER_PREFIX + "Unable to connectToServer(). State != tcp_init: " + std::to_string(state));
        setState(tcp_error);
        return false;
    }

    setState(tcp_connecting);

    // Setup read timeout:
    /*
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    */

    // Set socket nonblocking:
    // fcntl(sockFd, F_SETFL, O_NONBLOCK);

    log_info(LOGGER_PREFIX + "Socket connecting to " + hostAddr + " on port " + std::to_string(port) + "...");
    int ret = connect(sockFd, reinterpret_cast<struct sockaddr*>(&serverAddressStruct), sizeof(struct sockaddr));
    if (ret) {
        if (errno != EINPROGRESS) {
            log_error(LOGGER_PREFIX + "Socket connection failed with: " + std::string(strerror(errno)));
            setState(tcp_error);
            return false;
        }
    }
    log_info(LOGGER_PREFIX + "Connection established.");
    setState(tcp_connected);
    return true;
}

void TcpClient::disconnectFromServer() {
    setState(tcp_disconnecting);
    if (sockFd >= 0 && close(sockFd) < 0) {
        log_error(LOGGER_PREFIX + "Socket disconnect failed with: " + std::string(strerror(errno)));
        setState(tcp_error);
    } else {
        setState(tcp_disconnected);
    }
    sockFd = -1;
}

bool TcpClient::init() {
    if (state != tcp_disconnected) {
        log_error(LOGGER_PREFIX + "Unable to init(). State != tcp_disconnected: " + std::to_string(state));
        setState(tcp_error);
        return false;
    }

    if ((sockFd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        log_error(LOGGER_PREFIX + "Socket creation failed with: " + std::string(strerror(errno)));
        setState(tcp_error);
        return false;
    } else {
        memset(reinterpret_cast<char*>(&serverAddressStruct), 0, sizeof(serverAddressStruct));
        serverAddressStruct.sin_family = AF_INET;
        serverAddressStruct.sin_port = htons(port);

        // Look up the address of the server given its name:
        struct hostent *hp = gethostbyname(hostAddr.c_str());
        if (!hp) {
            log_error(LOGGER_PREFIX + "Unable to look up host: " + hostAddr);
            setState(tcp_error);
            return false;
        } else {
            // Add host address to the address structure:
            memcpy(reinterpret_cast<char*>(&serverAddressStruct.sin_addr), hp->h_addr_list[0], hp->h_length);
            log_debug(LOGGER_PREFIX + "Lookup for " + hostAddr + " => " + std::string(inet_ntoa((struct in_addr)*((struct in_addr*)hp->h_addr_list[0]))));
            setState(tcp_init);
            return true;
        }
    }
}

int TcpClient::readData(char* buf, size_t bufLength) {
    if (state != tcp_connected) {
        log_error(LOGGER_PREFIX + "Unable to readData(). State != tcp_connected: " + std::to_string(state));
        return -1;
    }

    return read(sockFd, buf, bufLength);
}

int TcpClient::sendData(char* buf, size_t bufLength) {
    if (state != tcp_connected) {
        log_error(LOGGER_PREFIX + "Unable to sendData(). State != tcp_connected: " + std::to_string(state));
        return -1;
    }

    return send(sockFd, buf, bufLength, 0);
}
