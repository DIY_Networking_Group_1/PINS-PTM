#pragma once

#include <string>
#include <iostream>
#include "Logger.h"

#ifdef __cplusplus
extern "C" {
#endif

#include <iwlib.h>

#ifdef __cplusplus
}
#endif

class WifiHelper {
 public:
    bool scan(const char* interface, wireless_scan_head* scanResult);
    bool tryConnect(const char* interface, const char* ssid, const char* pw);
    void connect(const char* interface, const char* ssid, const char* pw);
    bool scanForSsid(const char* interface, const char* ssid);
    bool isConnectedTo(const char* interface, const char* ssid);
    void init();
    void reset();
    void printScanResult(wireless_scan_head* scanResult);
    bool hasScanResultSsid(wireless_scan_head* scanResult, const char* ssid);

 private:
    const std::string LOGGER_PREFIX = "[WIFI_HELPER] ";
    int sockFd = -1;
};
