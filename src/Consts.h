#pragma once

#define HELPTEXT "pinsPtm -c <max transport cache size>\n\t-h prints this help"

#define DEFAULT_MAX_TRANSPORT_CACHE_SIZE 4096
#define DEFAULT_INTERFACE "wlan0"  // Raspberry: "wlp4s0"
#define DEFAULT_BASE_STATION_SSID "PINS-BM"  // PINS-BM
#define DEFAULT_BASE_STATION_PW "PINS-BM-123456789"
#define DEFAULT_BASE_STATION_IP "192.168.4.1"  // 192.168.4.1
#define DEFAULT_BASE_STATION_PORT 12345

#define TCP_MAX_READ_SIZE 2048
