#!/bin/bash

# Checks if the given 
INTERFACE=$1
SSID=$2
FOUND_EXIT_CODE=$3
NOT_FOUND_EXIT_CODE=$4

if [[ $(iw $INTERFACE scan 2>&1 | grep "SSID: $SSID") ]]; then
    # echo "Found $SSID"
    exit $FOUND_EXIT_CODE
else
    # echo "Not found $SSID"
    exit $NOT_FOUND_EXIT_CODE
fi