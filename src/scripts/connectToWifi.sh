#!/bin/bash

## NOTE: Assumes that the device is not in AP mode
## Instructions: https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

# Parameters
INTERFACE=$1
SSID=$2
PW=$3

# Bring down the interface
ifconfig wlan0 down

# Rewrite /etc/network/interfaces
# cp /etc/network/interfaces_sta /etc/network/interfaces

# Rewrite /etc/wpa_supplicant/wpa_supplicant.conf
cp files/wpa_supplicant_base.conf /etc/wpa_supplicant/wpa_supplicant.conf
echo 'network={' >> /etc/wpa_supplicant/wpa_supplicant.conf
echo '  ssid="$SSID"' >> /etc/wpa_supplicant/wpa_supplicant.conf
echo '  psk="$PW"' >> /etc/wpa_supplicant/wpa_supplicant.conf
echo '  #scan_ssid=1 # For hidden networks' >> /etc/wpa_supplicant/wpa_supplicant.conf
echo '}' >> /etc/wpa_supplicant/wpa_supplicant.conf

# Configure interface and enable dhcpclient:
wpa_supplicant -B -i$INTERFACE -c/etc/wpa_supplicant.conf -Dwext
dhclient $INTERFACE

# Rewrite dhcpcd.conf
# cp /etc/systemd/system/dhcpcd.service.d/dhcpcd_sta.conf /etc/systemd/system/dhcpcd.service.d/dhcpcd.conf

# Sleep to prevent wpa_supplicant from failing
sleep 1s

# Bring up the interface
ifconfig wlan0 up
