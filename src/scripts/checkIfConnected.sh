#!/bin/bash

# Checks if connected to the given Wifi network.
INTERFACE=$1
SSID=$2
CONNECTED_EXIT_CODE=$3
NOT_CONNECTED_EXIT_CODE=$4

if [[ $(iw $INTERFACE link 2>&1 | grep "SSID: $SSID") ]]; then
    echo "Connected to $SSID"
    exit $CONNECTED_EXIT_CODE
else
    echo "Not connected to $SSID"
    exit $NOT_CONNECTED_EXIT_CODE
fi
