#pragma once

#include <unistd.h>
#include <string>
#include "TcpClient.h"
#include "Consts.h"
#include "WifiHelper.h"
#include "messages/ptm/PtMessage.h"
#include "messages/MessageTools.h"
#include "Cache.h"

class CacheManager {
 public:
    explicit CacheManager(int maxCacheSize);
    ~CacheManager();
    void run(const char* interface, const char* ssid, const char* pw);

 private:
    const std::string LOGGER_PREFIX = "[CACHE_MANAGER] ";

    TcpClient client = TcpClient(DEFAULT_BASE_STATION_IP, DEFAULT_BASE_STATION_PORT);
    Cache* cache = NULL;
    uchar_sp baseStationAddr = NULL;

    void waitForSsid(WifiHelper* wifi, const char* interface, const char* ssid);
    bool connectToBaseStation();
    bool sendReceiveDataToBaseStation();
    int sendMessage(PtMessage* msg);
    bool readMessage(PtType type, PtMessage* msg, bool waitForNoMsgs);
    bool shouldExchangeWith(PtMessage* msg);
    void sendCache();
    void receiveCache();
};
