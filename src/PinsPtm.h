#pragma once

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string>
#include <iostream>
#include "Consts.h"
#include "CacheManager.h"

struct Args {
    uint32_t maxTransportCacheSize = DEFAULT_MAX_TRANSPORT_CACHE_SIZE;
};

int main(int argc, const char* argv[]);
static void parseArgs(int argc, const char* argv[], Args* argsRet);
void checkIfRunningAsRoot();
