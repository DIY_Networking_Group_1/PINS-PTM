#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <string>
#include <atomic>
#include "Consts.h"
#include "Logger.h"

enum TcpCState {
    tcp_disconnected = 0,
    tcp_init = 1,
    tcp_connecting = 2,
    tcp_connected = 3,
    tcp_disconnecting = 4,
    tcp_error = 5
};

class TcpClient {
 public:
    explicit TcpClient(std::string hostAddr, uint16_t port);
    ~TcpClient();
    bool connectToServer();
    void disconnectFromServer();
    bool init();
    int readData(char* buf, size_t bufLength);
    int sendData(char* buf, size_t bufLength);
    TcpCState getState();

 private:
    const std::string LOGGER_PREFIX = "[TCP_CLIENT] ";

    uint16_t port = 0;
    std::string hostAddr;
    sockaddr_in serverAddressStruct;
    int sockFd = -1;
    std::atomic<TcpCState> state{tcp_disconnected};

    void setState(TcpCState newState);
};
