#include "Logger.h"

void log_debug(std::string msg) {
    if (LOG_LEVEL >= l_debug) {
        std::cout << "[DEBUG]: " << msg << '\n';
    }
}

void log_info(std::string msg) {
    if (LOG_LEVEL >= l_info) {
        std::cout << "[INFO]: " << msg << '\n';
    }
}

void log_warn(std::string msg) {
    if (LOG_LEVEL >= l_warn) {
        std::cout << "[WARN]: " << msg << '\n';
    }
}

void log_error(std::string msg) {
    if (LOG_LEVEL >= l_error) {
        std::cerr << "[ERROR]: " << msg << std::endl;
    }
}
