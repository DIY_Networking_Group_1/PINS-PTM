#pragma once

#include <unordered_map>
#include <string>
#include <vector>
#include <memory>
#include <utility>
#include "Queue.h"
#include "Consts.h"
#include "messages/MessageTools.h"

struct CacheObj {
    size_t l;
    uchar_sp data;
};

typedef std::shared_ptr<Queue<CacheObj>> CacheQueue_spt;
typedef std::unordered_map<std::string, CacheQueue_spt> CacheMap;
typedef std::vector<std::pair<const std::string, const CacheQueue_spt>> ToSendCache;

class Cache {
 public:
    explicit Cache(int maxCacheSize);
    void add(uchar* addr, uchar_sp data, uint l);
    bool isFull();
    void getSendCache(uchar* addr, ToSendCache* toSendCache);

 private:
    CacheMap map = CacheMap();
    int cacheSize = 0;
    int maxCacheSize = DEFAULT_MAX_TRANSPORT_CACHE_SIZE;

    void add(std::string addr, uchar_sp data, uint l);
};
