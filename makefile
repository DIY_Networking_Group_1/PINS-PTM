G++_COMPILER=g++
G++_COMPILER_PI=/usr/local/gcc-8.1.0/bin/g++-8.1.0
BUILD_DIR=build
DEBUG_DIR=debug
BUILD_NAME=pinsPtm
TEST_NAME=$(BUILD_NAME)_test
MAIN_CPP=src/PinsPtm.cpp
MAIN_TEST=test/000-mainTest.cpp
ADDITONAL_CPP=src/messages/BinaryHelper.cpp src/messages/MessageTools.cpp src/WifiHelper.cpp src/Logger.cpp src/TcpClient.cpp src/CacheManager.cpp src/messages/ptm/PtMessage.cpp src/Cache.cpp
INCLUDE_DIR=-Isrc/ -Isrc/messages -Isrc/messages/ptm
ADDITIONAL_LIBS=-liw
ADDITIONAL_OPTIONS=$(ADDITIONAL_LIBS) -std=c++17 -pthread #-fsanitize=address -Wall

default:
	make clean
	make debug

init:
	git submodule init
	git submodule update --recursive --remote

run:
	make default
	./$(DEBUG_DIR)/$(BUILD_NAME)

compileCommands:
	mkdir -p $(DEBUG_DIR)
	clang++ -MJ a.o.json $(MAIN_CPP) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(DEBUG_DIR)/$(BUILD_NAME) $(ADDITIONAL_OPTIONS)
	clang++ -MJ b.o.json $(MAIN_TEST) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(DEBUG_DIR)/$(TEST_NAME) $(ADDITIONAL_OPTIONS)
	sed -e '1s/^/[\n/' -e '$$s/,$$/\n]/' *.o.json > compile_commands.json
	rm *.o.json

clean:
	# Only remove the build folder if it exists:
	if [ -d $(BUILD_DIR) ]; then rm -rf $(BUILD_DIR); fi
	# Only remove the debug folder if it exists:
	if [ -d $(DEBUG_DIR) ]; then rm -rf $(DEBUG_DIR); fi

compile:
	mkdir -p $(BUILD_DIR)
	${G++_COMPILER} -oo $(MAIN_CPP) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(BUILD_DIR)/$(BUILD_NAME) $(ADDITIONAL_OPTIONS)

compilePi:
	mkdir -p $(BUILD_DIR)
	${G++_COMPILER_PI} -oo $(MAIN_CPP) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(BUILD_DIR)/$(BUILD_NAME) $(ADDITIONAL_OPTIONS)

debug:
	mkdir -p $(DEBUG_DIR)
	${G++_COMPILER} -g $(MAIN_CPP) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(DEBUG_DIR)/$(BUILD_NAME) $(ADDITIONAL_OPTIONS)

runDebug:
	./$(DEBUG_DIR)/$(BUILD_NAME)

UnitTests:
	make clean
	make compileTest
	make runTests

compileTest:
	mkdir -p $(DEBUG_DIR)
	${G++_COMPILER} $(MAIN_TEST) $(ADDITONAL_CPP) $(INCLUDE_DIR) -o $(DEBUG_DIR)/$(TEST_NAME) $(ADDITIONAL_OPTIONS)

runTests:
	./$(DEBUG_DIR)/$(TEST_NAME)

install:
	mv $(BUILD_DIR)/$(BUILD_NAME) /opt/
	mkdir /opt/src/
	cp -r src/scripts/  /opt/src/
