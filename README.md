# PINS-PTM

## Description:
C/C++ PINS Raspberry Pi Portable transfer mode code.
Allows a RPI to act as a burst device transferring data from one [PINS-Base Station](https://gitlab.com/DIY_Networking_Group_1/PINS-BM) to an other [PINS-Base Station](https://gitlab.com/DIY_Networking_Group_1/PINS-BM).

## Dependencies:
* gcc >7.0
* [Wireless Tools v.29](https://hewlettpackard.github.io/wireless-tools/)

## Installation:
### gcc 8.1 for the Raspberry Pi:
Because the RPI doesn't support c++17 with it's slightly outdated gcc version, it has to get updated.
To check your version use `gcc --version`.
```
git clone https://bitbucket.org/sol_prog/raspberry-pi-gcc-binary.git
cd raspberry-pi-gcc-binary
tar xf gcc-8.1.0.tar.bz2
mv gcc-8.1.0 /usr/local
cd ..
echo 'export PATH=/usr/local/gcc-8.1.0/bin:$PATH' >> .bashrc
source .bashrc
rm -rf raspberry-pi-gcc-binary
```
To compile stuff with gcc 8.1 run for example: `g++-8.1.0 -std=c++17 -Wall -pedantic if_test.cpp -o if_test`

### Wireless Tools:
```
wget https://hewlettpackard.github.io/wireless-tools/wireless_tools.29.tar.gz
tar -xzf wireless_tools.29.tar.gz
rm -rf wireless_tools.29.tar.gz
cd wireless_tools.29
make
make install
```

### PINS-PTM:
```
git clone https://gitlab.com/DIY_Networking_Group_1/PINS-PTM.git
cd PINS-PTM/
make init
make compile # Use "make compilePi" if you would like to compile it on a RPI with an manually updated gcc version
```
